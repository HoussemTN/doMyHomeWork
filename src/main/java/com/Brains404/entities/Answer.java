package com.Brains404.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class Answer {
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
int id ; 


String content ; 
int rating  ;
@ManyToOne()
@JoinColumn(name="idAuthor")
Author author ; 
@ManyToOne()
@JoinColumn(name="idTask")
Task task ; 

public Answer() {
	super();
}

public Answer( int idTask, String content, int rating,int idAuthor) {
	super();
	this.author.setId(idAuthor);
	this.task.setId(idTask);
	this.content = content;
	this.rating = rating;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}


public String getContent() {
	return content;
}
public void setContent(String content) {
	this.content = content;
}
public int getRating() {
	return rating;
}
public void setRating(int rating) {
	this.rating = rating;
}
@Override
public String toString() {
	return "Answer [id=" + id + ", content=" + content + ", rating="
			+ rating + "]";
} 

}
