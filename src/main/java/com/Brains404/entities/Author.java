package com.Brains404.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Author {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	long id; 
	String name ; 
	String surname ; 
	int pointsAmount ;
	String password ; 
	String email ;
	@OneToMany(fetch = FetchType.LAZY,mappedBy="author")
	List<Task> tasks ; 
	@OneToMany(fetch = FetchType.LAZY,mappedBy="author")
	List<Task> answers ;
	public Author() {
		
	}

	public Author(String name, String surname, int pointsAmount, String password, String email) {
		super();
		this.name = name;
		this.surname = surname;
		this.pointsAmount = pointsAmount;
		this.password = password;
		this.email = email;
	}
	public Author(int id ,String name, String surname, int pointsAmount, String password, String email) {
		super();
		this.id=id; 
		this.name = name;
		this.surname = surname;
		this.pointsAmount = pointsAmount;
		this.password = password;
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public int getPointsAmount() {
		return pointsAmount;
	}
	public void setPointsAmount(int pointsAmount) {
		this.pointsAmount = pointsAmount;
	}

	@Override
	public String toString() {
		return "Author [id=" + id + ", name=" + name + ", surname=" + surname + ", pointsAmount=" + pointsAmount
				+ ", password=" + password + ", email=" + email + "]";
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	
	

	

}
