package com.Brains404.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Task {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id; 
	String title ; 
	String description ; 
	int points ; 
	int rating ; 
	Date dateCreation ;
	@ManyToOne()
	@JoinColumn(name="idAuthor")
	Author author ; 
	@OneToMany(fetch = FetchType.LAZY,mappedBy="task")
	List<Answer> answers ;
	
	
	public Task() {
		
	}
	
	public Task(int id, String title, String description, int points, int rating, Date dateCreation,int idAuthor) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.points = points;
		this.rating = rating;
		this.dateCreation = dateCreation;
	}
	public Task( String title, String description, int points, int rating, Date dateCreation,int idAuthor) {
		super();
		this.author.setId(idAuthor);
		this.title = title;
		this.description = description;
		this.points = points;
		this.rating = rating;
		this.dateCreation = dateCreation;
	}
	





	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public Date getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	@Override
	public String toString() {
		return "Task [id=" + id + ", title=" + title + ", description=" + description + ", points=" + points
				+ ", rating=" + rating + ", dateCreation=" + dateCreation + ", idAuthor=" + author + "]";
	}
	
	
}