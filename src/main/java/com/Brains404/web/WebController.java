package com.Brains404.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.Brains404.dao.AnswerRepository;
import com.Brains404.dao.AuthorRepository;
import com.Brains404.dao.TaskRepository;
import com.Brains404.entities.Author;

@Controller
public class WebController {
@Autowired
private AuthorRepository authorRepository ; 
@Autowired
private TaskRepository taskRepository ; 
@Autowired
private AnswerRepository answerRepository ;


@RequestMapping("author/add")
public String addAuthor(Model model) {

model.addAttribute("author",new Author());
return "addAuthor";
}


@RequestMapping(value="author/save",method=RequestMethod.POST)
public String saveAuthor(@Valid @ModelAttribute Author author,BindingResult bindingResult) {
	if(bindingResult.hasErrors()) {
		return "addAuthor";
	}
	
	authorRepository.save(author);
	return "redirect:/author/listAuthors";
}

@RequestMapping("/author/listAuthors")
public String listAuthors(Model model) {
model.addAttribute("authors",authorRepository.findAll());
return "listAuthors";
}


@RequestMapping("author/updateAuthor")
public String updateAuthor(Model model,Long id) {
	Author author= authorRepository.findById(id).get();
	model.addAttribute("author",author);
	return "updateAuthor";
}


@RequestMapping(value="author/update",method=RequestMethod.POST)
public String updateAuthor(@Valid @ModelAttribute Author author,BindingResult bindingResult) {
	if(bindingResult.hasErrors()) {
		return "updateAuthor";
	}
	
	authorRepository.save(author);
	return "redirect:/author/listAuthors";
}
@RequestMapping("author/deleteAuthor")
public String deleteAuthor(Model model,Long id) {
	Author author= authorRepository.findById(id).get();
	model.addAttribute("author",author);
	return "deleteAuthor";
}
@RequestMapping(value="author/delete",method=RequestMethod.POST)
public String deleteAuthor(@Valid @ModelAttribute Author author,BindingResult bindingResult) {
	if(bindingResult.hasErrors()) {
		return "redirect:/author/listAuthor";
	}
	authorRepository.delete(author);
	return "redirect:/author/listAuthors";
}



}

