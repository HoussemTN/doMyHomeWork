package com.Brains404.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.Brains404.dao.AnswerRepository;
import com.Brains404.dao.AuthorRepository;
import com.Brains404.dao.TaskRepository;
import com.Brains404.entities.Answer;
import com.Brains404.entities.Author;
import com.Brains404.entities.Task;

@RestController
public class ServiceController {
@Autowired
private AuthorRepository AuthorRepository ; 
@Autowired
private TaskRepository TaskRepository ; 
@Autowired
private AnswerRepository AnswerRepository ; 
@RequestMapping("Author/save")
public Author saveAuthor(Author p) { 
	AuthorRepository.save(p);
	return p ; 
	
}
@RequestMapping("Author/all")
public List<Author> getAuthors(){ 
	return AuthorRepository.findAll();
}




// Task Service  
@RequestMapping(value = "/{authorId}/task", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
public boolean saveTask( @PathVariable(value = "authorId") Long authorId,@RequestBody Task p) { 

     Author author1 = new Author();
     ArrayList<Task> tasks=new ArrayList<Task>();
     Optional<Author> byId = AuthorRepository.findById(authorId);
     if (!byId.isPresent()) {
       return false ; 
     }
     Author author = byId.get();

     //tie Author to Task
     p.setAuthor(author);

     Task task1 = TaskRepository.save(p);
     //tie Task to Author
     tasks.add(task1);
     author1.setTasks(tasks);

     return true;
	
}
@RequestMapping("Task/all")
public List<Task> getTasks(){ 
	return TaskRepository.findAll();
}

//Answer service 
@RequestMapping("Answer/all")
public List<Answer> getAnswers(){ 
	return AnswerRepository.findAll();
}
//TODO add save answer to service
}




