package com.Brains404.dao;



import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.Brains404.entities.Answer;
import com.Brains404.entities.Author;


public interface AnswerRepository extends JpaRepository<Answer,Long>{
	@Query("select a from Answer a where a.content like :x")
	public Page<Answer> proParMC(@Param("x")String mc ,Pageable p );
	public List<Answer> findByContent(String content );


}
