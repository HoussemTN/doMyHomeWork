package com.Brains404.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.Brains404.entities.Task;


public interface TaskRepository extends JpaRepository<Task,Long> {
	@Query("select t from Task t where t.title like :x")
	public Page<Task> TaskParPage(@Param("x")String mc ,Pageable p );
	public List<Task> findByTitle(String title ); 
    
		
		
}
