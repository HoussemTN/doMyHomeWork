package com.Brains404.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.Brains404.entities.Author;


public interface AuthorRepository extends JpaRepository<Author,Long> {
	@Query("select a from Author a where a.name like :x")
	public Page<Author> produitParMC(@Param("x")String mc ,Pageable p );
	public List<Author> findByName(String name );
	

		
		
}
