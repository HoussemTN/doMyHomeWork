package com.Brains404;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DoMyHomeWorkApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoMyHomeWorkApplication.class, args);
	}

}
